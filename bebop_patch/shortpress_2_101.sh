#!/bin/sh

ESSID=parrotLAN
DEFAULT_WIFI_SETUP=/sbin/broadcom_setup.sh

# Set light to orange
BLDC_Test_Bench -G 1 1 0 >/dev/null
BLDC_Test_Bench -M 2

# Check whether drone is in access point mode
if [ $(bcmwl ap) -eq 1 ]
then
        echo "Trying to connect to $ESSID" | logger -s -t "LongPress" -p user.info

        # Bring access point mode down
        $DEFAULT_WIFI_SETUP remove_net_interface
        # Configure wifi to connect to given essid
        ifconfig eth0 down
        bcmwl down
        bcmwl band auto
        bcmwl autocountry 1
        bcmwl up
        bcmwl ap 0
        bcmwl join ${ESSID}
        ifconfig eth0 up
        # Run dhpc client
        #udhcpc -b -i eth0 --hostname=$(hostname)
        ifconfig eth0 192.168.208.101 up
        route add default gw 192.168.208.1
        /bin/onoffbutton/shortpress_4.sh
        sleep 1
        BLDC_Test_Bench -M 1
else
        # Should make drone an access point again
        # Bug: does not work yet (turn drone off & on instead)
        $DEFAULT_WIFI_SETUP create_net_interface
fi

# Set light back to green after 1 second
(sleep 1; BLDC_Test_Bench -G 0 1 0 >/dev/null) &
