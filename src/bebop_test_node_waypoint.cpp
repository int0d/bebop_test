#include "ros/ros.h"
#include "std_msgs/Empty.h"
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>

int main(int argc, char **argv){
  ros::init(argc, argv, "bebop_test_node_waypoint");
  ros::NodeHandle n;
  std_msgs::Empty e;

  tf::TransformListener listener;
  tf::StampedTransform transform;

  ros::Publisher takeoff_pub = n.advertise<std_msgs::Empty>("/takeoff", 1);
  ros::Publisher land_pub = n.advertise<std_msgs::Empty>("/land", 1);
  ros::Publisher cmdvel_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
//  ros::Publisher motive_pub = n.advertise<geometry_msgs::>(" ", 1);

  ros::Rate loop_rate(10); // Hz

  takeoff_pub.publish(e);
  ROS_INFO("published 1");

  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();

  takeoff_pub.publish(e);
  ROS_INFO("published 2");

  geometry_msgs::Twist cmd_vel;
  cmd_vel.linear.x = 0.0;
  cmd_vel.linear.y = 0.0;
  cmd_vel.linear.z = 0.0;
  cmd_vel.angular.x = 0.0;
  cmd_vel.angular.y = 0.0;
  cmd_vel.angular.z = 0.0;

  int k = 10;
  double x_pos,y_pos,z_pos,x_ori,y_ori,z_ori,w_ori,vx,vy;

  geometry_msgs::Vector3 ref;
  ref.x = 0.0;
  ref.y = 0.0;
  ref.z = 0.0;



  while(ros::ok()){

    //cmdvel_pub.publish(cmd_vel);

    listener.lookupTransform("/world", "/parrot101",ros::Time(0), transform);
    x_pos = transform.getOrigin().x();
    y_pos = transform.getOrigin().y();
    z_pos = transform.getOrigin().z();
    x_ori = transform.getRotation().x();
    y_ori = transform.getRotation().y();
    z_ori = transform.getRotation().z();
    w_ori = transform.getRotation().w();
    ROS_INFO("%f %f %f %f %f %f %f",x_pos,y_pos,z_pos,x_ori,y_ori,z_ori,w_ori);

    vx = -k * (x_pos-ref.x);
    vy = -k * (y_pos-ref.y);

  }



  land_pub.publish(e);
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  land_pub.publish(e);
  printf("ros not ok (PARROT LAND)");

  return 0;
}
