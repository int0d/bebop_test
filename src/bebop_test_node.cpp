#include "ros/ros.h"
#include "std_msgs/Empty.h"
//#include "bebop_msgs/Ardrone3CameraStateOrientationV2.h"
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>

int main(int argc, char **argv){
  ros::init(argc, argv, "bebop_test_node");
  ros::NodeHandle n;
  std_msgs::Empty e;

  tf::TransformListener listener;
  tf::StampedTransform transform;

  ros::Publisher takeoff_pub = n.advertise<std_msgs::Empty>("/bebop/takeoff", 1);
  ros::Publisher land_pub = n.advertise<std_msgs::Empty>("/bebop/land", 1);
  //ros::Publisher camera_pub = n.advertise<bebop_msgs::Ardrone3CameraStateOrientationV2>("/bebop/states/ardrone3/CameraState/OrientationV2", 1);
  ros::Publisher cmdvel_pub = n.advertise<geometry_msgs::Twist>("/bebop/cmd_vel", 1);
  ros::Rate loop_rate(10); // Hz

  //takeoff_pub.publish(e);
  ROS_INFO("published 1");
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  //takeoff_pub.publish(e);
  ROS_INFO("published 2");
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();

  geometry_msgs::Twist cmd_vel;
  cmd_vel.linear.x = 0.0;
  cmd_vel.linear.y = 0.0;
  cmd_vel.linear.z = 0.0;
  cmd_vel.angular.x = 0.0;
  cmd_vel.angular.y = 0.0;
  cmd_vel.angular.z = 1.0;

  while(ros::ok()){
    //ROS_INFO("published");
    //takeoff_pub.publish(e);
    //cmdvel_pub.publish(cmd_vel);

    listener.lookupTransform("/global", "/ar_marker_1", ros::Time(0), transform);
    double x = transform.getOrigin().x();
    double y = transform.getOrigin().y();
    double z = transform.getOrigin().z();

    printf("%f, %f, %f\n", x, y, z);

    loop_rate.sleep();
  }



  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  //land_pub.publish(e);
  ROS_INFO("published 1");


  while(ros::ok()){
    //ROS_INFO("published");
    //takeoff_pub.publish(e);

    loop_rate.sleep();
  }

  return 0;
}
