#include "ros/ros.h"
#include "std_msgs/Empty.h"
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>

int main(int argc, char **argv){
  ros::init(argc, argv, "bebop_test_node_remap");
  ros::NodeHandle n;
  std_msgs::Empty e;

//  tf::TransformListener listener;
//  tf::StampedTransform transform;

  ros::Publisher takeoff_pub	= n.advertise<std_msgs::Empty>("/takeoff", 1);
  ros::Publisher land_pub	= n.advertise<std_msgs::Empty>("/land", 1);
  ros::Publisher cmdvel_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1);

  ros::Rate loop_rate(10); // Hz
//  ros::Rate loop_rate_turn(10); // Hz
  ros::Rate loop_rate_turn(1); // Hz
  ros::Rate loop_rate_wait(0.5);

  takeoff_pub.publish(e);
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  takeoff_pub.publish(e);
  ROS_INFO("published TAKEOFF"); 
 
  geometry_msgs::Twist cmd_vel;
  cmd_vel.linear.x = 0.0;
  cmd_vel.linear.y = 0.0;
  cmd_vel.linear.z = 0.0;
  cmd_vel.angular.x = 0.0;
  cmd_vel.angular.y = 0.0;
  cmd_vel.angular.z = 0.0;
  
  //while(ros::ok()){
    //ROS_INFO("published");
    //takeoff_pub.publish(e);
    //cmdvel_pub.publish(cmd_vel);

    //loop_rate.sleep();
  //}

	loop_rate_wait.sleep();
	loop_rate_wait.sleep();

	cmd_vel.linear.y=0.3;
	cmdvel_pub.publish(cmd_vel);
	loop_rate_turn.sleep();

	cmd_vel.linear.y=-0.3;
	cmdvel_pub.publish(cmd_vel);
	loop_rate_turn.sleep();
	cmdvel_pub.publish(cmd_vel);
	loop_rate_turn.sleep();


	cmd_vel.linear.y=0.3;
	cmdvel_pub.publish(cmd_vel);
	loop_rate_turn.sleep();
	cmdvel_pub.publish(cmd_vel);
	loop_rate_turn.sleep();

	cmd_vel.linear.y=0.0;
	cmd_vel.linear.x=0.3;
	cmdvel_pub.publish(cmd_vel);
	loop_rate_turn.sleep();
	cmdvel_pub.publish(cmd_vel);
	loop_rate_turn.sleep();

	cmd_vel.linear.x=-0.3;
	cmdvel_pub.publish(cmd_vel);
	loop_rate_turn.sleep();
	cmdvel_pub.publish(cmd_vel);
	loop_rate_turn.sleep();

	
  
  land_pub.publish(e);
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  land_pub.publish(e);
  ROS_INFO("published LAND");
  
//  while(ros::ok()){
    //ROS_INFO("published");
    //takeoff_pub.publish(e);

//    loop_rate.sleep();
// }

  return 0;
}
