#include "ros/ros.h"
#include "std_msgs/Empty.h"
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>

int main(int argc, char **argv){
  ros::init(argc, argv, "bebop_test_node_mul");
  ros::NodeHandle n;
  std_msgs::Empty e;

//  tf::TransformListener listener;
//  tf::StampedTransform transform;

  ros::Publisher takeoff_pub_100	= n.advertise<std_msgs::Empty>("/bebop100/takeoff", 1);
  ros::Publisher land_pub_100			= n.advertise<std_msgs::Empty>("/bebop100/land", 1);
  ros::Publisher takeoff_pub_101	= n.advertise<std_msgs::Empty>("/bebop101/takeoff", 1);
  ros::Publisher land_pub_101			= n.advertise<std_msgs::Empty>("/bebop101/land", 1);
  ros::Publisher cmdvel_pub_100 = n.advertise<geometry_msgs::Twist>("/bebop100/cmd_vel", 1);
  ros::Publisher cmdvel_pub_101 = n.advertise<geometry_msgs::Twist>("/bebop101/cmd_vel", 1);
  ros::Rate loop_rate(10); // Hz
  ros::Rate loop_rate_turn(10); // Hz
  ros::Rate loop_rate_wait(0.5);

  takeoff_pub_100.publish(e);
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  takeoff_pub_100.publish(e);
  ROS_INFO("published takeoff -> 100");
  
//  takeoff_pub_100.publish(e);
  
 
  takeoff_pub_101.publish(e);
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  takeoff_pub_101.publish(e);
  ROS_INFO("published takeoff -> 101");
  
//  takeoff_pub_101.publish(e);
 
  geometry_msgs::Twist cmd_vel;
  cmd_vel.linear.x = 0.0;
  cmd_vel.linear.y = 0.0;
  cmd_vel.linear.z = 0.0;
  cmd_vel.angular.x = 0.0;
  cmd_vel.angular.y = 0.0;
  cmd_vel.angular.z = 1.0;

  //while(ros::ok()){
    //ROS_INFO("published");
    //takeoff_pub.publish(e);
    //cmdvel_pub.publish(cmd_vel);

    //loop_rate.sleep();
  //}

	loop_rate_wait.sleep();
	loop_rate_wait.sleep();
	
	for(int i=0; i<35; i++){
		loop_rate_turn.sleep();
		cmdvel_pub_100.publish(cmd_vel);
		cmdvel_pub_101.publish(cmd_vel);
		ROS_INFO("-- TURN --");	
	}
	
	loop_rate_wait.sleep();
	loop_rate_wait.sleep();
  
  
  land_pub_100.publish(e);
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  land_pub_100.publish(e);
  ROS_INFO("published land -> 100");
  
//  land_pub_100.publish(e);
 
  land_pub_101.publish(e);
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  loop_rate.sleep();
  land_pub_101.publish(e);
  ROS_INFO("published land -> 101");


//  while(ros::ok()){
    //ROS_INFO("published");
    //takeoff_pub.publish(e);

//    loop_rate.sleep();
// }

  return 0;
}
